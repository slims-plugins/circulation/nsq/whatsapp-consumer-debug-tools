<?php
require 'vendor/autoload.php';

$def = new Symfony\Component\Dotenv\Dotenv();
$def->loadEnv(__DIR__.'/.env');

#var_dump($_ENV); die();

$nsqlookupd_url = $_ENV['NSQLOOKUPD_HOST'].':'.$_ENV['NSQLOOKUPD_PORT'];
$nsq_lookupd = new NsqLookupd($nsqlookupd_url); //the nsqlookupd http addr
$nsq = new Nsq();

$config = array(
  "topic" => $_ENV['TOPIC'],
  "channel" => $_ENV['CHANNEL'],
  "rdy" => (INT)$_ENV['RDY'],                //optional , default 1
  "connect_num" => (INT)$_ENV['CONNECT_NUM'],        //optional , default 1   
  "retry_delay_time" => (INT)$_ENV['RETRY_DELAY_TIME'],  //optional, default 0 , if run callback failed, after 5000 msec, message will be retried
  "auto_finish" => $_ENV['AUTO_FINISH'], //default true

);

$nsq->subscribe($nsq_lookupd, $config, function($msg,$bev){
    $circ = json_decode($msg->payload);

    # DEBUG NSQ
    echo "=====\n";
    echo $msg->payload."\n";
    #echo $msg->attempts."\n";
    #echo $msg->messageId."\n";
    #echo $msg->timestamp."\n";
    #$message = 'Tes. Bismillah. no.11';
    var_dump($circ);

});




